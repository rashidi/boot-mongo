package my.zin.rashidi.mongo.user;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * @author Rashidi Zin
 * @since 1.0.0-SNAPSHOT
 */
public interface UserRepository extends MongoRepository<User, String> {

    User findOneByUsername(String username);

    List<User> findAllByNameLikeIgnoringCase(String name);
}
